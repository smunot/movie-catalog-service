package com.srm.moviecatalogservice.models;

public class Ratings {

    int rateing;
    String name;
    public Ratings() {
    }
    public Ratings(int rateing, String name) {
        this.rateing = rateing;
        this.name = name;
    }

    public int getRateing() {
        return rateing;
    }

    public void setRateing(int rateing) {
        this.rateing = rateing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
