package com.srm.moviecatalogservice.models;

import java.util.List;

public class UserRating {
    public List<Ratings> getRatings() {
        return ratings;
    }

    public void setRatings(List<Ratings> ratings) {
        this.ratings = ratings;
    }

    List<Ratings> ratings;

}
