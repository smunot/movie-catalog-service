package com.srm.moviecatalogservice.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.srm.moviecatalogservice.config.DbSettings;
import com.srm.moviecatalogservice.models.Movie;
import com.srm.moviecatalogservice.models.MovieCatalog;
import com.srm.moviecatalogservice.models.Ratings;
import com.srm.moviecatalogservice.models.UserRating;
import com.srm.moviecatalogservice.services.MovieInfoService;
import com.srm.moviecatalogservice.services.RatingInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
//import org.springframework.web.reactive.function.client.WebClient;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
@RefreshScope
public class MovieCatalogController {

//    @Autowired
//    RestTemplate restTemplate;

    @Autowired
    MovieInfoService movieInfoService;

    @Autowired
    RatingInfoService ratingInfoService;

    @Value("${my.greeting:Greeting not available}")
    String greeting;
    @Value("static msg")
    String staticMsg;

    @Value("${my.list}")
    List<String> list;

//    @Value("${my.map}")
//    Map<String, String> map;

    @Autowired
    DbSettings dbSettings;

    //    @Autowired
//    WebClient.Builder builder;
//    @HystrixCommand(fallbackMethod = "getFallBack")
    @GetMapping("/movieCatalogs/{userId}")
    public ResponseEntity<?> getMovieCatalogs(@PathVariable("userId") String userId) {
        UserRating ratings = ratingInfoService.getRatingInfo(userId);
//        UserRating ratings = restTemplate.getForObject("http://localhost:8585/rating/getRatings/"+userId, UserRating.class);

        return ResponseEntity.ok().body(ratings.getRatings().stream().map(m -> {
            Movie movie = movieInfoService.getMovieCatalogs(m);
            return new MovieCatalog(movie.getName(), "ok ok", movie.getId());
        }).collect(Collectors.toList()));
    }

    @GetMapping("greeting")
    public ResponseEntity<?> getGreeting() {
        return ResponseEntity.ok().body(greeting + staticMsg + list/*+  map*/ +
                dbSettings.getConnection() + dbSettings.getHost() + dbSettings.getPort());
    }

//    public ResponseEntity<?> getFallBack(@PathVariable("userId") String userId) {
//        return ResponseEntity.ok().body(Arrays.asList(new MovieCatalog("no Movie", "ok ok", 00)));
//    }

    //            Movie movie = builder.build()
//                    .get()
//                    .uri("http://localhost:8787/movie/movieInfo/" + m.getId())
//                    .retrieve()
//                    .bodyToMono(Movie.class)
//                    .block();
}

