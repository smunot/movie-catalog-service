package com.srm.moviecatalogservice.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.srm.moviecatalogservice.models.Ratings;
import com.srm.moviecatalogservice.models.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@Service
public class RatingInfoService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getFallBackRatingInfo")
    public UserRating getRatingInfo(String userId){
        UserRating userRating = restTemplate.getForObject("http://RATING-DATA-SERVICE/rating/getRatings/" + userId, UserRating.class);
        return userRating;
    }

    public UserRating getFallBackRatingInfo(String userId){
        UserRating userRating = new UserRating();
        userRating.setRatings(Arrays.asList(new Ratings(0,"Movie not found")));
        return userRating;
    }
}
