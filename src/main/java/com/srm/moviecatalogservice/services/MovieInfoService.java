package com.srm.moviecatalogservice.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.srm.moviecatalogservice.models.Movie;
import com.srm.moviecatalogservice.models.MovieCatalog;
import com.srm.moviecatalogservice.models.Ratings;
import com.srm.moviecatalogservice.models.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
@Service
public class MovieInfoService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getFallBackMovieCatalog")
    public Movie getMovieCatalogs(Ratings ratings){

            Movie movie = restTemplate.getForObject("http://MOVIE-INFO-SERVICE/movie/movieInfo/" + ratings.getRateing(), Movie.class);
            return movie;
    }

    public Movie getFallBackMovieCatalog(Ratings ratings){
        return new Movie(00,"no Movie");
    }

}
