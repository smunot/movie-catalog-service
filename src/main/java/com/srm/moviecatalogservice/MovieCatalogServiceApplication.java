package com.srm.moviecatalogservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;

@EnableSwagger2
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@EnableHystrixDashboard
public class MovieCatalogServiceApplication extends SpringBootServletInitializer {
    public static final String SERVER_PORT = "server.port";

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieCatalogServiceApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(MovieCatalogServiceApplication.class);
    }

    public static void main(String[] args) throws UnknownHostException {

        SpringApplication app = new SpringApplication(MovieCatalogServiceApplication.class);
        Environment env = app.run(args).getEnvironment();
        LOGGER.info("\n------------------------------------\n\t"
                        + "Aplication '{}' is running on! Access URLs:\n\t" + "Local:\t\thttp://localhost:{}\n\t"
                        + "External: \thttp://{}:{}\n---------------------------------",
                env.getProperty("spring.application.name"), env.getProperty(SERVER_PORT),
                InetAddress.getLocalHost().getHostAddress(), env.getProperty(SERVER_PORT));
    }


    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

//    @Bean
//    public WebClient.Builder getWebClient(){
//        return  WebClient.builder();
//    }
}
